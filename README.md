# Debug Dispositivo real en Google Chrome
Se usa para ver los posibles errores que se obtenga al momento de ejecutar una aplicación desarrollado en ionic en un dispositivo móvil real.

* Para ello lo primero que se debe hacer es conectar el dispositivo mediante USB y realizar el deploy en dicho dispositivo móvil real, para ello se usa el comando:
```
ionic cordova run --livereload
```
* Una vez ejecutado el comando, abrimos el navegador Google Chrome.

![Google Chrome](imagenes/google-chrome.png)

* A continuación damos click derecho y seleccionamos inspeccionar.

![Google Chrome](imagenes/google-chromeinspeccionar.png)

* Nos dirigimos a seleccionar los 3 puntitos en la parte superior derecha

![opciones](imagenes/3-puntitos.png)

* Seleccionamos **More Tools** y escogemos la opción de **Remote Devices**

![remote device](imagenes/remote-devices.png)

* A continuación se observará el dispositivo conectado

![remote device](imagenes/device.png)

* Damos click en el dispositivo.

![device](imagenes/device2.png)

* Una vez realizado lo anterior, a continuación, debemos tener inicializada la aplicación en el dispositivo móvil real, nos aparecerá la dirección en la pestaña de **remote devices**, con lo cual se debe dar click a botón **Inspect**.

![device](imagenes/device3.png)

* Se abrirá otra pantalla, la cual contendrá la interfaz de la aplicación, para poder ver la consola y los posibles errores, nos dirigimos a consola.

![device](imagenes/device4.png)



<a href="https://twitter.com/kevi_n_24" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @kevi_n_24 </a><br>